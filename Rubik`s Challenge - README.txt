Uma simula��o de um Cubo de Rubik. Suporte a visualiza��o 3D-Anaglifo (Vermelho - Verde).

Controles

Teclas de Controle 

	ESC - Sai do Jogo
	F9 - Ativa/Desativa o Modo Full-Screen
	F11 - Diminui o N�vel de Anti-Aliasing
	F12 - Aumenta o N�vel de Anti-Aliasing


Teclas para a rotacionar o cubo

	Flecha para Baixo - Rota��o negativa em torno do Eixo Y

	Flecha para Cima - Rota��o positiva em torno do Eixo Y

	Flecha para Direita - Rota��o positiva em torno do Eixo X

	Flecha para Esquerda - Rota��o negativa em torno do Eixo X

Teclas para a rotacionar o cubo at� uma face

	A - Rotaciona para a face de Tr�s (Laranja)

	S - Rotaciona para a face da Esquerda (Branca)

	D - Rotaciona para a face da Frente (Vermelha)

	F - Rotaciona para a face da Direita (Amarela)

	E - Rotaciona para a face do Topo (Azul)

	C - Rotaciona para a face do Baixo (Verde)

Teclas para Sele��o e Rota��o de Camadas

	*** As opera��es de sele��o e rota��o ser�o executadas sempre na face mais vis�vel.
	    Para rotacionar, mantenha o botao de sele��o pressionado e execute a rota��o.

		Ex: Pressione e segure a "8" e ap�s tecle "7" (Rota��o de camada superior para direita).

	8 - Selecionara a camada mais acima da face.
		7 - Rotaciona a camada mais acima para esquerda
		9 - Rotaciona a camada mais acima para direita

	2 - Selecionara a camada mais abaixo da face.
		1 - Rotaciona a camada mais abaixo para esquerda
		3 - Rotaciona a camada mais abaixo para direita

	4 - Selecionara a camada mais a esquerda da face.
		7 - Rotaciona a camada mais a esquerda para cima
		1 - Rotaciona a camada mais a esquerda para baixo

	6 - Selecionara a camada mais a direita da face.
		9 - Rotaciona a camada mais a direita para cima
		3 - Rotaciona a camada mais a direita para baixo

Teclas de Controle do Modo Anaglifo

	F10 - Ativa/Desativa o Modo Anaglifo
	
	+ (Tecla Adi��o)- Aumenta a sepera��o das imagens (aproxima o cubo do rosto do observador).

	- (Tecla Subtra��o)- Diminui a sepera��o das imagens (aproxima o cubo do tela do computador).
     





