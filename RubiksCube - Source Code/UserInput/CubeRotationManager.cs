﻿using System;

using RubiksChallenge.Entities.CubeStructure;
using RubiksChallenge.Entities.CubeStructure.Layers;
using RubiksChallenge.Geometry;

namespace RubiksChallenge.UserInput
{
    public class CubeRotationManager
    {
        #region Constructor

        private CubeRotationManager()
        {
        }

        #endregion

        #region Singleton

        private static CubeRotationManager instance;

        public static CubeRotationManager GetManager()
        {
            if (instance == null)
                instance = new CubeRotationManager();
            return instance;
        }

        #endregion

        #region Attributes and Properties

        private float rotationSpeed = 5.0f;
        public float RotationSpeed
        {
            get { return this.rotationSpeed; }
            set 
            {
                if (value > 1.0f)
                    value = 1.0f;
                this.rotationSpeed = value; 
            }
        }

        #endregion        

        #region Public Methods

        public void ExecuteDown()
        {
            RubiksCube.GetRubiksCube().Position.Rotate(Vector3D.XAxis, this.rotationSpeed);
        }

        public void ExecuteLeft()
        {
            RubiksCube.GetRubiksCube().Position.Rotate(Vector3D.YAxis, - this.rotationSpeed);
        }

        public void ExecuteRight()
        {
            RubiksCube.GetRubiksCube().Position.Rotate(Vector3D.YAxis, this.rotationSpeed);
        }

        public void ExecuteUp()
        {
            RubiksCube.GetRubiksCube().Position.Rotate(Vector3D.XAxis, - this.rotationSpeed);
        }        

        #endregion
    }
}