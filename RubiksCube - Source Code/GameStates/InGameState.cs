﻿using System;
using System.Collections.Generic;

using RubiksChallenge.Entities;
using RubiksChallenge.Entities.CubeStructure;
using RubiksChallenge.Environment;
using RubiksChallenge.Properties;
using RubiksChallenge.Textures;
using RubiksChallenge.UserInput;

using Tao.Glfw;
using Tao.OpenGl;

namespace RubiksChallenge.GameStates
{
    public class InGameState : IGameState, IEntityManager 
    {
        #region Constructor

        public InGameState()
        {
        }

        #endregion

        #region Attributes and Properties

        private List<AbstractEntity> addList = new List<AbstractEntity>();
        public List<AbstractEntity> AddList
        {
            get { return this.addList; }
            set { this.addList = value; }
        }

        private List<AbstractEntity> entities = new List<AbstractEntity>();
        public List<AbstractEntity> Entities
        {
            get { return this.entities; }
            set { this.entities = value; }
        }

        private List<AbstractEntity> removeList = new List<AbstractEntity>();
        public List<AbstractEntity> RemoveList
        {
            get { return this.removeList; }
            set { this.removeList = value; }
        }        

        public String GetName
        {
            get { return "ingamestate"; }
        }

        private Texture backgroundTexture;
        public Texture BackgroundTexture
        {
            get { return this.backgroundTexture; }
        }

        #endregion

        #region Public Methods

        #region IGameState Members

        public void Enter(GameManager manager)
        {
            this.Entities.Clear();

            this.AddEntity(RubiksCube.GetRubiksCube());
        }   

        public void Init(GameManager manager)
        {
            this.backgroundTexture = TextureLoader.GetTextureLoader().LoadTexture(Resources.Background);            
        }

        public void Leave(GameManager manager)
        {
        }

        public void Render(GameManager manager)
        {
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
            Gl.glClearColor(0.3f, 0.3f, 0.3f, 1.0f);

            this.DrawBackground();            
            Gl.glLoadIdentity();
            Glu.gluLookAt(0.0f, 0.0f, 7.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

            for (int i = 0; i < this.entities.Count; i++)
                this.entities[i].Render();

            Glfw.glfwSwapBuffers();            
        }

        public void RenderAnaglyphStereoscopy(GameManager manager)
        {
            AnaglyphStereoscopy anag = AnaglyphStereoscopy.GetAnaglyphStereoscopy();
            
            int width = 0;
            int height = 0;
            Glfw.glfwGetWindowSize(out width, out height);

            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
            this.DrawAnaglyphStereoscopyBackground();

            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();
            Glu.gluLookAt(anag.CameraVP.X + anag.RightVector.A,
                          anag.CameraVP.Y + anag.RightVector.B,
                          anag.CameraVP.Z + anag.RightVector.C,
                          anag.CameraVP.X + anag.RightVector.A + anag.CameraVD.A,
                          anag.CameraVP.Y + anag.RightVector.B + anag.CameraVD.B,
                          anag.CameraVP.Z + anag.RightVector.C + anag.CameraVD.C,
                          anag.CameraVU.A,
                          anag.CameraVU.B,
                          anag.CameraVU.C);
            
            // THE REAL DEAL
            //Glu.gluLookAt(anag.CameraVP.X + anag.RightVector.A, anag.CameraVP.Y, anag.CameraVP.Z, 0, 0, 0, 0, 1, 0);
            Gl.glColorMask(Gl.GL_FALSE, Gl.GL_FALSE, Gl.GL_TRUE, Gl.GL_TRUE);            
            for (int i = 0; i < this.entities.Count; i++)
                this.entities[i].Render();

            Gl.glClear(Gl.GL_DEPTH_BUFFER_BIT);
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();
            Glu.gluLookAt(anag.CameraVP.X - anag.RightVector.A,
                          anag.CameraVP.Y - anag.RightVector.B,
                          anag.CameraVP.Z - anag.RightVector.C,
                          anag.CameraVP.X - anag.RightVector.A + anag.CameraVD.A,
                          anag.CameraVP.Y - anag.RightVector.B + anag.CameraVD.B,
                          anag.CameraVP.Z - anag.RightVector.C + anag.CameraVD.C,
                          anag.CameraVU.A,
                          anag.CameraVU.B,
                          anag.CameraVU.C);
            
            // THE REAL DEAL
            //Glu.gluLookAt(anag.CameraVP.X - anag.RightVector.A, anag.CameraVP.Y, anag.CameraVP.Z, 0, 0, 0, 0, 1, 0);
            Gl.glColorMask(Gl.GL_TRUE, Gl.GL_FALSE, Gl.GL_FALSE, Gl.GL_TRUE);
            for (int i = 0; i < this.entities.Count; i++)
                this.entities[i].Render();

            Glfw.glfwSwapBuffers();
            Gl.glColorMask(Gl.GL_TRUE, Gl.GL_TRUE, Gl.GL_TRUE, Gl.GL_TRUE);
        } 

        public void ReInit(GameManager manager)
        {
            Scene.GetScene().ReInitialize();
            RubiksCube.GetRubiksCube().ReInit();

            this.Init(manager);

            //RubiksCube RU = new RubiksCube();

            //RU.Position.Translate(4.0f, 0.0f, -7.0f);
            //this.AddEntity(RU);
        }

        public void Update(GameManager manager)
        {
            for (int i = 0; i < this.removeList.Count; i++)
                this.entities.Remove(this.removeList[i]);
            for (int i = 0; i < this.addList.Count; i++)
                this.entities.Add(this.addList[i]);

            this.RemoveList.Clear();
            this.AddList.Clear();

            for (int i = 0; i < this.entities.Count; i++)
                this.entities[i].Update(this);

            if (AnaglyphStereoscopy.GetAnaglyphStereoscopy().Active)
                UserInputManager.GetManager().HandleAnaglyphStereoscopy();
        }

        #endregion

        #region IEntity Manager Members

        public void AddEntity(AbstractEntity entity)
        {
            this.AddList.Add(entity);
        }        

        public void RemoveEntity(AbstractEntity entity)
        {
            this.RemoveList.Add(entity);
        }        

        #endregion

        #endregion

        #region Private Methods

        private void DrawBackground()
        {
            int width = 0;
            int height = 0;
            Glfw.glfwGetWindowSize(out width, out height);

            this.EnterOrtho();

            this.BackgroundTexture.Bind();

            Gl.glBegin(Gl.GL_QUADS);
            Gl.glTexCoord2f(0, 0);
            Gl.glVertex2i(0, 0);
            Gl.glTexCoord2f(0, 1);
            Gl.glVertex2i(0, height);
            Gl.glTexCoord2f(1, 1);
            Gl.glVertex2i(width, height);
            Gl.glTexCoord2f(1, 0);
            Gl.glVertex2i(width, 0);
            Gl.glEnd();

            this.LeaveOrtho();
        }

        private void DrawAnaglyphStereoscopyBackground()
        {
            AnaglyphStereoscopy anag = AnaglyphStereoscopy.GetAnaglyphStereoscopy();

            int width = 0;
            int height = 0;
            Glfw.glfwGetWindowSize(out width, out height);

            this.EnterOrtho();

            this.BackgroundTexture.Bind();

            Gl.glColorMask(Gl.GL_FALSE, Gl.GL_FALSE, Gl.GL_TRUE, Gl.GL_TRUE);
            Gl.glBegin(Gl.GL_QUADS);
            Gl.glTexCoord2f(0, 0);
            Gl.glVertex2f(0 - ((float)anag.CameraEyesep * -10.0f), 0);
            Gl.glTexCoord2f(0, 1);
            Gl.glVertex2f(0 - ((float)anag.CameraEyesep * -10.0f), height);
            Gl.glTexCoord2f(1, 1);
            Gl.glVertex2f(width - ((float)anag.CameraEyesep * -10.0f), height);
            Gl.glTexCoord2f(1, 0);
            Gl.glVertex2f(width - ((float)anag.CameraEyesep * -10.0f), 0);
            Gl.glEnd();

            Gl.glColorMask(Gl.GL_TRUE, Gl.GL_FALSE, Gl.GL_FALSE, Gl.GL_TRUE);
            Gl.glBegin(Gl.GL_QUADS);
            Gl.glTexCoord2f(0, 0);
            Gl.glVertex2f(0 + ((float)anag.CameraEyesep * +10.0f), 0);
            Gl.glTexCoord2f(0, 1);
            Gl.glVertex2f(0 + ((float)anag.CameraEyesep * +10.0f), height);
            Gl.glTexCoord2f(1, 1);
            Gl.glVertex2f(width + ((float)anag.CameraEyesep * +10.0f), height);
            Gl.glTexCoord2f(1, 0);
            Gl.glVertex2f(width + ((float)anag.CameraEyesep * +10.0f), 0);
            Gl.glEnd();

            this.LeaveOrtho();
        }

        private void EnterOrtho()
        {
            int width = 0;
            int height = 0;

            Glfw.glfwGetWindowSize(out width, out height);

            Gl.glPushAttrib(Gl.GL_DEPTH_BUFFER_BIT | Gl.GL_ENABLE_BIT);
            Gl.glPushMatrix();
            Gl.glLoadIdentity();
            Gl.glMatrixMode(Gl.GL_PROJECTION);
            Gl.glPushMatrix();

            Gl.glLoadIdentity();
            Glu.gluOrtho2D(0, width, 0, height);
            Gl.glDisable(Gl.GL_DEPTH_TEST);
            Gl.glDisable(Gl.GL_LIGHTING);  
        }

        private void LeaveOrtho()
        {
            Gl.glPopMatrix();
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glEnable(Gl.GL_DEPTH_TEST);
            Gl.glEnable(Gl.GL_LIGHTING);
            Gl.glPopMatrix();
            Gl.glPopAttrib();            
        }

        #endregion
    }
}
