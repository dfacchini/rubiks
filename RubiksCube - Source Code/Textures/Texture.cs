using System;

using Tao.OpenGl;

namespace RubiksChallenge.Textures
{
    public class Texture
    {
        #region Constructor

        public Texture(int target, int textureID)
        {
            this.Target = target;
            this.TextureID = textureID;
        }

        #endregion

        #region Attributes and Properties

        private int target;
        public int Target 
        {
            get { return this.target;  }
            set { this.target = value; }
        }

        private int textureID;
        public int TextureID
        {
            get { return this.textureID; }
            set { this.textureID = value; }
        }

        #endregion        

        #region Public Methods

        public void Bind()
        {            
            Gl.glBindTexture(this.Target, this.TextureID);                    
        }

        #endregion
    }
}
