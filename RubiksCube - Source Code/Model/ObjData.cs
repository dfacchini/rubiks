using System;
using System.Collections.Generic;
using System.IO;

using RubiksChallenge.Geometry;

namespace RubiksChallenge.Model
{
    public class ObjData 
    {
        #region Constructor

        public ObjData(StreamReader reader)
        {
            while (!reader.EndOfStream)
            {
                String line = reader.ReadLine();
                line = line.Replace(".", ",");
                if (line == null)
                    break;

                if (line.StartsWith("vn"))
                {
                    Point3D normal = this.ReadPoint3D(line);
                    this.normals.Add(normal);
                }
                else if (line.StartsWith("vt"))
                {
                    Point2D tex = this.ReadPoint2D(line);
                    this.texCoords.Add(tex);
                }
                else if (line.StartsWith("v"))
                {
                    Point3D vert = this.ReadPoint3D(line);
                    this.verts.Add(vert);
                }
                else if (line.StartsWith("f"))
                {
                    Face face = this.ReadFace(line);
                    this.Faces.Add(face);
                }
            }
        }
        
        #endregion

        #region Private Fields

        private List<Point3D> verts = new List<Point3D>();
        private List<Point3D> normals = new List<Point3D>();
        private List<Point2D> texCoords = new List<Point2D>();

        #endregion        

        #region Attributes and Properties

        private List<Face> faces;
        public List<Face> Faces
        {
            get
            {
                if (this.faces == null)
                    this.faces = new List<Face>();
                return this.faces;
            }
        }

        public int FaceCount
        {
            get { return Faces.Count; }
        }

        #endregion        
	
        #region Private Methods

        private Point3D ReadPoint3D(String line)
        {
            string[] tuple3 = line.Split(' ');

            float x = float.Parse(tuple3[1]);
            float y = float.Parse(tuple3[2]);
            float z = float.Parse(tuple3[3]);

            return new Point3D(x, y, z);
        }

        private Point2D ReadPoint2D(String line)
        {
            string[] tuple2 = line.Split(' ');

            float x = float.Parse(tuple2[1]);
            float y = float.Parse(tuple2[2]);

            return new Point2D(x, y);
        }

        private Face ReadFace(String line)
        {
            string[] readFace = line.Split(' ');

            Face face = new Face(readFace.Length - 1);

            foreach (string facePoints in readFace)
            {
                if (string.Equals(facePoints, "f"))
                    continue;                
                string[] points = facePoints.Split('/');
                int v = int.Parse(points[0]);
                int t = int.Parse(points[1]);
                int n = int.Parse(points[2]);

                face.AddPoint((Point3D)verts[v -1], (Point2D)texCoords[t -1], (Point3D)normals[n -1]);
            }

            return face;
        } 

        #endregion
    }
}
