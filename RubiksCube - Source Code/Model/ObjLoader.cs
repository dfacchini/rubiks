using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

using RubiksChallenge.Textures;

namespace RubiksChallenge.Model
{
    public class ObjLoader
    {
        #region Constructor

        private ObjLoader()
        {            
        }

        #endregion

        #region Singleton

        private static ObjLoader instance;

        public static ObjLoader GetLoader()
        {
            if (instance == null)
                instance = new ObjLoader();
            return instance;
        }
        
        #endregion

        #region Public Methods

        public ObjModel LoadObj(string resourceName, Bitmap resourceBitmap, Bitmap anaglyphStereoscopyResourceBitmap)
        {
            StreamReader streamReader = new StreamReader(this.GetType().Module.Assembly.GetManifestResourceStream(resourceName));
            Texture texture = TextureLoader.GetTextureLoader().LoadTexture(resourceBitmap);
            Texture anaglyphStereoscopyTexture = TextureLoader.GetTextureLoader().LoadTexture(anaglyphStereoscopyResourceBitmap);

            return new ObjModel(new ObjData(streamReader), texture, anaglyphStereoscopyTexture);
        }

        #endregion
    }
}
