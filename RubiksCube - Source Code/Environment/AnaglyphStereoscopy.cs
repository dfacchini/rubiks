﻿using System;
using System.Drawing;

using RubiksChallenge.Geometry;
using RubiksChallenge.UserInput;

using Tao.Glfw;

namespace RubiksChallenge.Environment
{
    public class AnaglyphStereoscopy
    {
        #region Constructor

        private AnaglyphStereoscopy()
        {
            this.Initialize();
            
        }

        #endregion

        #region Singleton

        private static AnaglyphStereoscopy instance;

        public static AnaglyphStereoscopy GetAnaglyphStereoscopy()
        {
            if (instance == null)
                instance = new AnaglyphStereoscopy();
            return instance;
        }

        #endregion

        #region Attributes and Properties

        private bool active = false;
        public bool Active
        {
            get { return this.active; }
        }

        private double cameraEyesep = 0.0;
        public double CameraEyesep
        {
            get { return cameraEyesep; }
        }

        private Vector3D cameraVD = new Vector3D(0.0f, 0.0f, -1.0f);
        public Vector3D CameraVD
        {
            get { return this.cameraVD; }
        }

        private Point3D cameraVP = new Point3D(0.0f, 0.0f, 7.0f);
        public Point3D CameraVP
        {
            get { return this.cameraVP; }
        }

        private Vector3D cameraVU = new Vector3D(0.0f, 1.0f, 0.0f);
        public Vector3D CameraVU
        {
            get { return this.cameraVU; }
        }

        private Vector3D rightVector;
        public Vector3D RightVector
        {
            get { return rightVector; }            
        }

        #endregion

        #region Private Methods

        private void Initialize()
        {
            this.rightVector = Vector3D.CrossProduct(this.CameraVD, this.CameraVU);                
            this.rightVector.Normalize();
            this.rightVector.Multiply((float)(this.cameraEyesep / 2.0));
        }

        #endregion

        #region Public Methods

        public void Activate()
        {
            this.active = true;
            this.cameraEyesep = 0.0;
            this.Initialize();
        }

        public void Deactivate()
        {
            this.active = false;
        }

        public void IncreaseEyeSeparation()
        {
            this.cameraEyesep += 0.01f;
            this.Initialize();
        }

        public void RelaxEyeSeparation()
        {
            this.cameraEyesep -= 0.01f;
            this.Initialize();
        }

        public void ResetEyeSeparation()
        {
            this.cameraEyesep = 0.0f;
            this.Initialize();
        }

        #endregion
    }
}
