using System;
using RubiksChallenge.Entities.CubeStructure;

namespace RubiksChallenge.Geometry
{
	public class Position
    {
        #region Constructor

        public Position()
		{
            this.UpdatePosition();            
		}

        public Position(Point3D point)
        {
            this.UpdatePosition();
            this.Translate(point.X, point.Y, point.Z);
        }

        public Position(Matrix3D matrix)
        {
            this.tMatrix = matrix;
            this.UpdatePosition();
        }

        #endregion

        #region Attributes and Properties

        private Vector3D axis;
        public Vector3D Axis
        {
            get { return this.axis; }
            set { this.axis = value; }
        }

        private Point3D location;
        public Point3D Location
        {
            get { return this.location; }
            set { this.location = value; }
        }

        private Matrix3D tMatrix;
        public Matrix3D TMatrix
        {
            get { return this.tMatrix; }
        }

        private Matrix3D rMatrix;
        public Matrix3D RMatrix
        {
            get { return this.rMatrix; }
        }

        private float rotation;
        public float Rotation
        {
            get { return this.rotation; }
            set { this.rotation = value; }
        }

        #endregion        

        #region Private Fields
        
        private Quaternion quaternion;

        #endregion

        #region Private Methods

        private void UpdatePosition()
        {
            if (this.tMatrix == null)
                this.tMatrix = new Matrix3D();
            if (this.rMatrix == null)
                this.rMatrix = new Matrix3D();

            this.quaternion = this.rMatrix.ExtractRotation();
            this.quaternion.GetAxisAngle(out this.axis, out this.rotation);
            this.rotation = (float)Math3D.Deg(rotation);
            
            Vector3D loc = this.tMatrix.ExtractTranslation();            
            this.location = new Point3D(loc.A, loc.B, loc.C);
        }

        #endregion

        #region Public Methods

        public Position Clone()
        {
            Matrix3D matrix = new Matrix3D(this.tMatrix);
            return new Position(matrix);
        }

        public void Rotate(Vector3D rotationAxis, float rotationAngle)
        {
            this.rMatrix.Rotate(rotationAxis, (float)Math3D.Rad(rotationAngle));
            this.UpdatePosition();
        }

        public void RotateAndTranslate(Vector3D rotationAxis, float rotationAngle)
        {
            Vector3D origin = new Vector3D(this.Location.X, this.Location.Y, this.Location.Z);
            Vector3D destination = new Vector3D(this.Location.X, this.Location.Y, this.Location.Z);

            Matrix3D matrix = new Matrix3D();
            matrix.Rotate(rotationAxis, -(float)Math3D.Rad(rotationAngle));
            destination.Transform(matrix);

            Vector3D translationVector = new Vector3D(destination.A - origin.A,
                                                      destination.B - origin.B,
                                                      destination.C - origin.C);
            
            this.TMatrix.Translate(translationVector);            
            this.RMatrix.Rotate(rotationAxis, (float)Math3D.Rad(rotationAngle));
            
            this.UpdatePosition();
        }

        public void Translate(float x, float y, float z)
        {
            this.tMatrix.Translate(x, y, z);
            this.UpdatePosition();
        }        

        #endregion
    }
}
