using System;

namespace RubiksChallenge.Geometry
{
    public class Face
    {
        #region Constructor

        public Face(int points)
        {
            verts = new Point3D[points];
            norms = new Point3D[points];
            texs = new Point2D[points];
        }

        #endregion        

        #region Private Fields

        private int points;

        #endregion                

        #region Attributes and Properties

        private Point3D[] verts;
        public Point3D[] Vertex
        {
            get { return this.verts; }
        }

        private Point3D[] norms;
        public Point3D[] Normal
        {
            get { return this.norms; }
        }

        private Point2D[] texs;
        public Point2D[] TexCoord
        {
            get { return this.texs; }
        } 

        #endregion        

        #region Public Methods

        public void AddPoint(Point3D vert, Point2D tex, Point3D norm)
        {
            this.Vertex[points] = vert;
            this.TexCoord[points] = tex;
            this.Normal[points] = norm;

            points++;
        }
        
        #endregion
    }
}
