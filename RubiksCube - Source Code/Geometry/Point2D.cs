using System;

namespace RubiksChallenge.Geometry
{
    public class Point2D
    {
        #region Constructor

        public Point2D()
        {
            this.X = 0.0f;
            this.Y = 0.0f;
        }

        public Point2D(float x, float y)
        {
            this.X = x;
            this.Y = y;
        }
        
        #endregion

        #region Attributes and Properties

        private float x;
        public float X
        {
            get { return this.x; }
            set { this.x = value; }
        }

        private float y;
        public float Y
        {
            get { return this.y; }
            set { this.y = value; }
        }

        #endregion        

        #region Public Methods

        public float Angle(Point2D point)
        {
            float angle = 0.0f;
            double radAngle;

            double diffX = point.X - this.X;
            double diffY = point.Y - this.Y;
            
            if (diffX == 0)
            {
                if (point.Y > this.Y)
                    return angle = 90.0f;
                else
                    return angle = 270.0f;
            }
            else
            {
                radAngle = Math.Atan(diffY / diffX);
                if (diffX < 0)
                    radAngle += Math.PI;

                return angle = (float)(radAngle * 180.0d / Math.PI);
            }           
        }

        #endregion
    }
}
