using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Xml;
using System.Runtime.InteropServices;

namespace RubiksChallenge.Geometry
{
    /// <summary>
    /// Represents a plane in 3-space.
    /// xA + yB + zC - D = 0
    /// </summary>
    public struct Plane3D : ICloneable
    {
        //--------------------------------------------------------------------------------

        /// <summary>
        /// Create a new plane
        /// </summary>
        /// <param name="normal"></param>
        /// <param name="constant"></param>
        public Plane3D(Vector3D normal, float constant)
        {
            this.Normal = normal;
            this.Constant = constant;
        }

        //--------------------------------------------------------------------------------

        /// <summary>
        /// Create the plane that has the given normal and is coplanar with the given point
        /// </summary>
        /// <param name="normal"></param>
        /// <param name="coplanarPoint"></param>
        /// <returns></returns>
        static public Plane3D FromNormalAndPoint(Vector3D normal, Vector3D coplanarPoint)
        {
            return new Plane3D(normal.GetUnit(), Vector3D.Dot(normal, coplanarPoint));
        }

        /// <summary>
        /// Create the plane defined by the three given points
        /// </summary>
        /// <param name="pt0"></param>
        /// <param name="pt1"></param>
        /// <param name="pt2"></param>
        /// <returns></returns>
        static public Plane3D FromCoplanarPoints(Vector3D pt0, Vector3D pt1, Vector3D pt2)
        {
            Vector3D normal = Vector3D.Cross(pt2 - pt1, pt0 - pt1).GetUnit();
            return new Plane3D(normal, Vector3D.Dot(normal, pt0));
        }


        object ICloneable.Clone()
        {
            Plane3D plane = new Plane3D();
            plane.Constant = this.Constant;
            plane.Normal = (Vector3D)this.Normal.Clone();
            return plane;
        }

        /// <summary>
        /// Copy
        /// </summary>
        /// <returns></returns>
        public Plane3D Clone()
        {
            return new Plane3D(this.Normal, this.Constant);
        }

        //--------------------------------------------------------------------------------

        /// <summary>
        /// The Normal parameter of the plane when defined as "Normal*( pt ) - Constant = 0" 
        /// </summary>
        [XmlIgnore]
        public Vector3D Normal;

        /// <summary>
        /// The Constant parameter of the plane when defined as "Normal*( pt ) - Constant = 0"
        /// </summary>
        [XmlIgnore]
        public float Constant;

        /// <summary>
        /// The A parameter of the plane when defined as "Ax + By + Cz - D = 0"
        /// </summary>
        [XmlAttribute]
        public float A
        {
            get { return Normal.A; }
            set { Normal.A = value; }
        }

        /// <summary>
        /// The B parameter of the plane when defined as "Ax + By + Cz - D = 0"
        /// </summary>
        [XmlAttribute]
        public float B
        {
            get { return Normal.B; }
            set { Normal.B = value; }
        }

        /// <summary>
        /// The C parameter of the plane when defined as "Ax + By + Cz - D = 0"
        /// </summary>
        [XmlAttribute]
        public float C
        {
            get { return Normal.C; }
            set { Normal.C = value; }
        }

        /// <summary>
        /// The D parameter of the plane when defined as "Ax + By + Cz - D = 0"
        /// </summary>
        [XmlAttribute]
        public float D
        {
            get { return Constant; }
            set { Constant = value; }
        }

        //--------------------------------------------------------------------------------

        /// <summary>
        /// Flip the plane.
        /// </summary>
        public void Flip()
        {
            Normal = -Normal;
            Constant = -Constant;
        }

        /// <summary>
        /// Create a new plane that is identical the current but flipped.
        /// </summary>
        /// <returns></returns>
        public Plane3D GetFlipped()
        {
            Plane3D plane = (Plane3D)this.Clone();
            plane.Flip();
            return plane;
        }

        /// <summary>
        /// Get the shortest distance from the point to the plane
        /// </summary>
        /// <param name="pt"></param>
        /// <returns></returns>
        public float GetDistanceToPlane(Vector3D pt)
        {
            return Vector3D.Dot(pt, Normal) - Constant;
        }

        /// <summary>
        /// Get the sign of the point in relation to the plane
        /// </summary>
        /// <param name="pt"></param>
        /// <returns></returns>
        public Sign GetSign(Vector3D pt)
        {
            return Math3D.GetSign(GetDistanceToPlane(pt), Math3D.EpsilonF);
        }

        /// <summary>
        /// Project a point onto the plane
        /// </summary>
        /// <param name="pt"></param>
        /// <returns></returns>
        public Vector3D ProjectOntoPlane(Vector3D pt)
        {
            return pt + (Constant - Vector3D.Dot(pt, Normal)) * Normal;
        }

        /// <summary>
        /// Determine whether the line connecting pt0 to pt1 intersects the plane
        /// </summary>
        /// <param name="pt0"></param>
        /// <param name="pt1"></param>
        /// <returns></returns>
        public bool IsIntersection(Vector3D pt0, Vector3D pt1)
        {
            int sign0 = (int)Math3D.GetSign(GetDistanceToPlane(pt0));
            int sign1 = (int)Math3D.GetSign(GetDistanceToPlane(pt1));

            return (sign0 < 0 && sign1 > 0) || (sign0 > 0 && sign1 <= 0);
        }

        /// <summary>
        /// Determine the exact location where the given line segment intersects the plane
        /// </summary>
        /// <param name="pt0"></param>
        /// <param name="pt1"></param>
        /// <returns></returns>
        public Vector3D GetIntersection(Vector3D pt0, Vector3D pt1)
        {
            Debug.Assert(IsIntersection(pt0, pt1) == true);

            Vector3D ptDirection = pt1 - pt0;
            float denominator = Vector3D.Dot(Normal, ptDirection);
            if (denominator == 0)
            {
                throw new DivideByZeroException("Can not get the intersection of a plane with a line when they are parallel to each other.");
            }
            float t = (Constant - Vector3D.Dot(Normal, pt0)) / denominator;
            return pt0 + ptDirection * t;
        }

        //--------------------------------------------------------------------------------

        /// <summary>
        /// Are two planes equal?
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        static public bool operator ==(Plane3D a, Plane3D b)
        {
            return (a.Constant == b.Constant) && (a.Normal == b.Normal);
        }

        /// <summary>
        /// Are two planes not equal?
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        static public bool operator !=(Plane3D a, Plane3D b)
        {
            return (a.Constant != b.Constant) || (a.Normal != b.Normal);
        }

        /// <summary>
        /// Is given object equal to current planes?
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public override bool Equals(object o)
        {
            if (o is Plane3D)
            {
                Plane3D plane = (Plane3D)o;
                return (this.Constant == plane.Constant) && (this.Normal == plane.Normal);
            }
            return false;
        }

        /// <summary>
        /// Get the hashcode of the planes
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return this.Constant.GetHashCode() ^ this.Normal.GetHashCode();
        }

        //--------------------------------------------------------------------------------

        /// <summary>
        /// Get an orthogonal basis for the plane
        /// </summary>
        /// <param name="u"></param>
        /// <param name="v"></param>
        public void GetBasis(out Vector3D u, out Vector3D v)
        {
            Vector3D pt = this.ProjectOntoPlane(Vector3D.Origin);

            u = this.ProjectOntoPlane(pt + Vector3D.XAxis) - pt;
            u = Vector3D.Max(u, this.ProjectOntoPlane(pt + Vector3D.YAxis) - pt);
            u = Vector3D.Max(u, this.ProjectOntoPlane(pt + Vector3D.ZAxis) - pt);

            v = Vector3D.Cross(u, this.Normal);

            u.Normalize();
            v.Normalize();
        }

        // ================================================================================

        /// <summary>
        /// Get a description of the plane.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "Plane3D [ n=" + Normal.ToString() + " c=" + Constant + " ]";
        }

        /// <summary>
        /// Zero
        /// </summary>
        static public readonly Plane3D Zero = new Plane3D();
    }
}
