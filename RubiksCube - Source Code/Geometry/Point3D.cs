using System;

namespace RubiksChallenge.Geometry
{
	public class Point3D
    {
        #region Constructor

        public Point3D()
		{
            this.X = 0.0f;
            this.Y = 0.0f;
            this.Z = 0.0f;
		}

		public Point3D(float x, float y, float z)
		{
			this.X = x; 
            this.Y = y; 
            this.Z = z;
        }

        #endregion

        #region Attributes and Properties

        private float x;
        public float X
		{
			get { return this.x; }
			set { this.x = value; }
		}
        
        private float y;
		public float Y
		{
			get { return this.y; }
			set { this. y = value; }
		}

        private float z;
		public float Z
		{
			get { return this.z; }
			set { this.z = value; }
        }

        #endregion

        #region Public Methods

        public float Distance(Point3D point)
        {
            float xDiff = point.X - this.X;
            float yDiff = point.Y - this.Y;
            float zDiff = point.Z - this.Z;

            return (float)Math.Sqrt(Math.Pow(xDiff, 2) + 
                                    Math.Pow(yDiff, 2) + 
                                    Math.Pow(zDiff, 2));
        }

        #endregion
    }	
}