﻿using System;

namespace RubiksChallenge.Entities.CubeStrucutre.Factory
{
    public enum CubeType
    {
        SingleFaced,
        DoubleFaced,
        TripleFaced
    }
}
