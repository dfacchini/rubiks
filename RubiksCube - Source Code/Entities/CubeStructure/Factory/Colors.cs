using System;

namespace RubiksChallenge.Entities.CubeStructure.Factory
{
    public enum Colors
    {
        Black,
        Blue,
        Green,
        Orange,
        Red,
        White,
        Yellow
    }
}
