﻿using System;
using System.Collections.Generic;

using RubiksChallenge.Entities.CubeStructure.Factory;
using RubiksChallenge.Entities.CubeStructure.Layers;
using RubiksChallenge.Environment;
using RubiksChallenge.Geometry;
using RubiksChallenge.UserInput;

using Tao.Glfw;
using Tao.OpenGl;

namespace RubiksChallenge.Entities.CubeStructure
{
    public class RubiksCube : AbstractEntity
    {
        #region Constructor

        public RubiksCube()
            : base()
        {
            RubiksCubeAssembler.Assembly(this);
        }

        #endregion

        #region Singleton

        private static RubiksCube instance;

        public static RubiksCube GetRubiksCube()
        {
            if (instance == null)
                instance = new RubiksCube();
            return instance;
        }

        #endregion

        #region Attributes and Properties

        private Cube[, ,] cubes;
        public Cube[, ,] Cubes
        {
            get
            {
                if (this.cubes == null)
                    this.cubes = new Cube[3, 3, 3];
                return this.cubes;
            }
            set { this.cubes = value; }
        }        

        private bool layerRotating = false;
        public bool LayerRotating
        {
            get { return this.layerRotating; }
            private set { this.layerRotating = value; }
        }

        private bool rotatingToLayer = false;
        public bool RotatingToLayer
        {
            get { return this.rotatingToLayer; }
            set { this.rotatingToLayer = value; }
        }

        private bool resetingRotation = false;
        public bool ResetingRotation
        {
            get { return this.resetingRotation; }
            set { this.resetingRotation = value; }
        }

        private AbstractLayer selectedLayer;        
        public AbstractLayer SelectedLayer
        {
            get { return this.selectedLayer; }
            set
            {
                if (this.SelectedLayer == value) return;
                if (this.selectedLayer != null)
                    this.selectedLayer.UnselectLayer();
                this.selectedLayer = value;
                if (this.selectedLayer != null)
                    this.selectedLayer.SelectLayer();
            }
        }
        
        #endregion

        #region Private Fields

        private RotationDirection rotationDirection;
        
        private Vector3D rotateToVector;        
        private float rotateToAngle;

        private int step = 0;        

        #endregion

        #region Overriden Methods

        public override void Render()
        {
            Gl.glPushMatrix();
            
            Gl.glTranslated(this.Position.Location.X, this.Position.Location.Y, this.Position.Location.Z);
            Gl.glRotatef(this.Position.Rotation, this.Position.Axis.A, this.Position.Axis.B, this.Position.Axis.C);
            
            for (int x = 0; x < 3; x++)
                for (int y = 0; y < 3; y++)
                    for (int z = 0; z < 3; z++)
                        if (this.Cubes[x, y, z] != null)
                            if (!this.Cubes[x, y, z].Selected)
                                this.Cubes[x, y, z].Render();

            if (this.SelectedLayer != null)
                this.SelectedLayer.RenderSelectedLayer();

            Gl.glPopMatrix();
        }

        public override void ReInit()
        {
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                    for (int k = 0; k < 3; k++)
                        if (this.cubes[i, j, k] != null)
                            this.cubes[i, j, k].ReInit();
        }

        public override void Update(IEntityManager manager)
        {
            if (this.LayerRotating)
            {
                this.RotateLayer();
                return;
            }
            if (this.RotatingToLayer)
            {
                this.RotateToLayer();
                return;
            }

            UserInputManager.GetManager().HandleRotation();
            UserInputManager.GetManager().HandleLayerSelection();
        }

        #endregion

        #region Public Methods

        public void RotateLayer(RotationDirection rotationDirection)
        {
            this.rotationDirection = rotationDirection;
            this.LayerRotating = true;
            this.step = 0;            
        }

        public void RotateToLayer(Vector3D rotationVector, float rotationAngle)
        {
            this.ResetingRotation = true;
            this.RotatingToLayer = true;
            this.rotateToVector = rotationVector;
            this.rotateToAngle = rotationAngle;
            this.RotateToLayer();
        }
                
        #endregion

        #region Private Methods

        private void RotateLayer()
        {
            if (this.SelectedLayer != null)
            {
                if (this.layerRotating)
                {
                    this.SelectedLayer.Rotate(this.rotationDirection, step);
                    this.step += 10;
                    if (this.step > 90)
                        this.LayerRotating = false;
                }                
            }
        }

        private void ResetRotation()
        {
            float step = CubeRotationManager.GetManager().RotationSpeed;

            if (this.ResetingRotation)
            {
                Vector3D rotationVector = this.Position.Axis;
                float rotationAngle = this.Position.Rotation;

                if (rotationAngle == 0)
                    this.ResetingRotation = false;
                else if (rotationAngle >= step)
                    this.Position.Rotate(rotationVector, -step);
                else
                    this.Position.Rotate(rotationVector, -rotationAngle);
            }            
        }

        private void RotateToLayer()
        {
            float step = CubeRotationManager.GetManager().RotationSpeed;

            if (this.ResetingRotation)
                this.ResetRotation();
            else
            {
                if (this.RotatingToLayer)
                {
                    Vector3D rotationVector = this.Position.Axis;
                    float rotationAngle = this.Position.Rotation;

                    if ((Math3D.FloatComparison(rotationVector.A, this.rotateToVector.A, Math3D.EpsilonF)) &&
                        (Math3D.FloatComparison(rotationVector.B, this.rotateToVector.B, Math3D.EpsilonF)) &&
                        (Math3D.FloatComparison(rotationVector.C, this.rotateToVector.C, Math3D.EpsilonF)) &&
                        (rotationAngle == this.rotateToAngle))
                    {
                        this.RotatingToLayer = false;
                        return;
                    }

                    if ((this.rotateToAngle - rotationAngle) >= step)
                        this.Position.Rotate(this.rotateToVector, step);
                    else
                        this.Position.Rotate(this.rotateToVector, (this.rotateToAngle - rotationAngle));
                }
            }
        }
        
        #endregion
    }
}
