﻿using System;

namespace RubiksChallenge.Entities
{
    public interface IDrawable
    {
        #region Interface Methods

        void Render();

        void Update();

        #endregion
    }
}
