using System;

namespace RubiksChallenge.GameStates
{
    public interface IGameState 
    {
        #region Interface Methods

        void Enter(GameManager manager);

        String GetName
        {
            get;
        }

        void Init(GameManager manager);

        void Leave(GameManager manager);

        void ReInit(GameManager manager);

        void Render(GameManager manager);

        void RenderAnaglyphStereoscopy(GameManager manager);

        void Update(GameManager manager);               

        #endregion
    }
}
