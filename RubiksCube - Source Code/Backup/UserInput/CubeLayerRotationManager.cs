﻿using System;

using RubiksChallenge.Entities.CubeStructure;
using RubiksChallenge.Entities.CubeStructure.Layers;
using RubiksChallenge.Geometry;
using System.Collections.Generic;

namespace RubiksChallenge.UserInput
{
    public class CubeLayerRotationManager
    {
        #region Constructor

        private CubeLayerRotationManager()
        {
        }

        #endregion

        #region Singleton

        private static CubeLayerRotationManager instance;

        public static CubeLayerRotationManager GetManager()
        {
            if (instance == null)
                instance = new CubeLayerRotationManager();
            return instance;
        }

        #endregion

        #region Attributes and Properties

        private AbstractLayer backLayer;
        public AbstractLayer BackLayer
        {
            get 
            { 
                if (this.backLayer == null)
                    this.backLayer = new BackLayer();
                return this.backLayer; 
            }
        }

        private AbstractLayer bottomLayer;
        public AbstractLayer BottomLayer
        {
            get 
            { 
                if (this.bottomLayer == null)
                    this.bottomLayer = new BottomLayer();
                return this.bottomLayer; 
            }
        }

        private AbstractLayer frontLayer;
        public AbstractLayer FrontLayer
        {
            get 
          { 
            if (this.frontLayer == null)
                this.frontLayer = new FrontLayer();
            return this.frontLayer; 
          }
        }

        private AbstractLayer leftLayer;
        public AbstractLayer LeftLayer
        {
            get 
          { 
              if (this.leftLayer == null)
                  this.leftLayer = new LeftLayer();
              return leftLayer; 
          }
        }

        private AbstractLayer rightLayer;
        public AbstractLayer RightLayer
        {
            get 
          { 
              if (this.rightLayer == null)
                  this.rightLayer = new RightLayer();
              return rightLayer; 
          }
        }

        private AbstractLayer topLayer;
        public AbstractLayer TopLayer
        {
          get 
          { 
              if (this.topLayer == null)
                  this.topLayer = new TopLayer();
              return topLayer; 
          }
        }

        private List<AbstractLayer> layers;
        public List<AbstractLayer> Layers
        {
            get 
            {
                if (this.layers == null)
                {
                    this.layers = new List<AbstractLayer>();
                    this.layers.Add(this.BackLayer);
                    this.layers.Add(this.BottomLayer);
                    this.layers.Add(this.FrontLayer);
                    this.layers.Add(this.LeftLayer);
                    this.layers.Add(this.RightLayer);
                    this.layers.Add(this.TopLayer);
                }
                return this.layers; 
            }
        }

        #endregion

        #region Private Methods

        private AbstractLayer CalculateNearestLayer(Point3D locationPoint)
        {
            AbstractLayer activeLayer = null;
            //float nearestDistance = 3.0f;
            //float compareDistance = 0.0f;

            //Position backPosition = this.BackLayer.GetCube(this.BackLayer.MiddlePosition).Position.Clone();
            //backPosition.AddCubeRotation();
            //compareDistance = backPosition.Location.Distance(locationPoint);
            //if (compareDistance < 0)
            //    compareDistance *= -1;
            //if (compareDistance < nearestDistance)
            //{
            //    nearestDistance = compareDistance;
            //    activeLayer = this.BackLayer;
            //}

            //Position bottomPosition = this.BottomLayer.GetCube(this.BottomLayer.MiddlePosition).Position.Clone();
            //bottomPosition.AddCubeRotation();
            //compareDistance = bottomPosition.Location.Distance(locationPoint);
            //if (compareDistance < 0)
            //    compareDistance *= -1;
            //if (compareDistance < nearestDistance)
            //{
            //    nearestDistance = compareDistance;
            //    activeLayer = this.BottomLayer;
            //}

            //Position frontPosition = this.FrontLayer.GetCube(this.FrontLayer.MiddlePosition).Position.Clone();
            //frontPosition.AddCubeRotation();
            //compareDistance = frontPosition.Location.Distance(locationPoint);
            //if (compareDistance < 0)
            //    compareDistance *= -1;
            //if (compareDistance < nearestDistance)
            //{
            //    nearestDistance = compareDistance;
            //    activeLayer = this.FrontLayer;
            //}

            //Position leftPosition = this.LeftLayer.GetCube(this.LeftLayer.MiddlePosition).Position.Clone();
            //leftPosition.AddCubeRotation();
            //compareDistance = leftPosition.Location.Distance(locationPoint);
            //if (compareDistance < 0)
            //    compareDistance *= -1;
            //if (compareDistance < nearestDistance)
            //{
            //    nearestDistance = compareDistance;
            //    activeLayer = this.LeftLayer;
            //}

            //Position rightPosition = this.RightLayer.GetCube(this.RightLayer.MiddlePosition).Position.Clone();
            //rightPosition.AddCubeRotation();
            //compareDistance = rightPosition.Location.Distance(locationPoint);
            //if (compareDistance < 0)
            //    compareDistance *= -1;
            //if (compareDistance < nearestDistance)
            //{
            //    nearestDistance = compareDistance;
            //    activeLayer = this.RightLayer;
            //}

            //Position topPosition = this.TopLayer.GetCube(this.TopLayer.MiddlePosition).Position.Clone();
            //topPosition.AddCubeRotation();
            //compareDistance = topPosition.Location.Distance(locationPoint);
            //if (compareDistance < 0)
            //    compareDistance *= -1;
            //if (compareDistance < nearestDistance)
            //{
            //    nearestDistance = compareDistance;
            //    activeLayer = this.TopLayer;
            //}

            return activeLayer;
        }

        #endregion

        #region Public Methods

        #region Layer Rotation Methods

        public void ExecuteBottomPositiveRotation()
        {
            if (RubiksCube.GetRubiksCube().LayerRotating) return;
            AbstractLayer layer = RubiksCube.GetRubiksCube().SelectedLayer;
            if (layer != null)
            {
                if ((layer == this.BottomLayer) |
                   (layer == this.FrontLayer) |
                   (layer == this.RightLayer))
                    RubiksCube.GetRubiksCube().RotateLayer(RotationDirection.Negative);
                else
                    RubiksCube.GetRubiksCube().RotateLayer(RotationDirection.Positive);
            }
        }

        public void ExecuteBottomNegativeRotation()
        {
            if (RubiksCube.GetRubiksCube().LayerRotating) return;
            AbstractLayer layer = RubiksCube.GetRubiksCube().SelectedLayer;
            if (layer != null)
            {
                if ((layer == this.BottomLayer) |
                   (layer == this.FrontLayer) |
                   (layer == this.RightLayer))
                    RubiksCube.GetRubiksCube().RotateLayer(RotationDirection.Positive);
                else
                    RubiksCube.GetRubiksCube().RotateLayer(RotationDirection.Negative);
            }
        }

        public void ExecuteLeftPositiveRotation()
        {
            if (RubiksCube.GetRubiksCube().LayerRotating) return;
            AbstractLayer layer = RubiksCube.GetRubiksCube().SelectedLayer;
            if (layer != null)
            {
                if ((layer == this.LeftLayer) |
                   (layer == this.BackLayer) |
                   (layer == this.TopLayer))
                    RubiksCube.GetRubiksCube().RotateLayer(RotationDirection.Negative);
                else
                    RubiksCube.GetRubiksCube().RotateLayer(RotationDirection.Positive);
            }
        }

        public void ExecuteLeftNegativeRotation()
        {
            if (RubiksCube.GetRubiksCube().LayerRotating) return;
            AbstractLayer layer = RubiksCube.GetRubiksCube().SelectedLayer;
            if (layer != null)
            {
                if ((layer == this.LeftLayer) |
                   (layer == this.BackLayer) |
                   (layer == this.TopLayer))
                    RubiksCube.GetRubiksCube().RotateLayer(RotationDirection.Positive);
                else
                    RubiksCube.GetRubiksCube().RotateLayer(RotationDirection.Negative);
            }
        }

        public void ExecuteRightPositiveRotation()
        {
            if (RubiksCube.GetRubiksCube().LayerRotating) return;
            AbstractLayer layer = RubiksCube.GetRubiksCube().SelectedLayer;
            if (layer != null)
            {
                if ((layer == this.RightLayer) |
                   (layer == this.FrontLayer) |
                   (layer == this.BottomLayer))
                    RubiksCube.GetRubiksCube().RotateLayer(RotationDirection.Negative);
                else
                    RubiksCube.GetRubiksCube().RotateLayer(RotationDirection.Positive);
            }
        }

        public void ExecuteRightNegativeRotation()
        {
            if (RubiksCube.GetRubiksCube().LayerRotating) return;
            AbstractLayer layer = RubiksCube.GetRubiksCube().SelectedLayer;
            if (layer != null)
            {
                if ((layer == this.RightLayer) |
                   (layer == this.FrontLayer) |
                   (layer == this.BottomLayer))
                    RubiksCube.GetRubiksCube().RotateLayer(RotationDirection.Positive);
                else
                    RubiksCube.GetRubiksCube().RotateLayer(RotationDirection.Negative);
            }
        }

        public void ExecuteTopPositiveRotation()
        {
            if (RubiksCube.GetRubiksCube().LayerRotating) return;
            AbstractLayer layer = RubiksCube.GetRubiksCube().SelectedLayer;
            if (layer != null)
            {
                if ((layer == this.TopLayer) |
                   (layer == this.BackLayer) |
                   (layer == this.LeftLayer))
                    RubiksCube.GetRubiksCube().RotateLayer(RotationDirection.Negative);
                else
                    RubiksCube.GetRubiksCube().RotateLayer(RotationDirection.Positive);
            }
        }

        public void ExecuteTopNegativeRotation()
        {
            if (RubiksCube.GetRubiksCube().LayerRotating) return;
            AbstractLayer layer = RubiksCube.GetRubiksCube().SelectedLayer;
            if (layer != null)
            {
                if ((layer == this.TopLayer) |
                   (layer == this.BackLayer) |
                   (layer == this.LeftLayer))
                    RubiksCube.GetRubiksCube().RotateLayer(RotationDirection.Positive);
                else
                    RubiksCube.GetRubiksCube().RotateLayer(RotationDirection.Negative);
            }
        } 

        #endregion

        #region Layer Rotation Selection Methods

        public void ExecuteSelectBottom()
        {
            if (RubiksCube.GetRubiksCube().LayerRotating) return;

            float y = 0.0f;
            AbstractLayer retLayer = null;

            foreach (AbstractLayer layer in this.Layers)
            {
                Position pos = layer.GetCube(layer.MiddlePosition).Position.Clone();
                Vector3D location = new Vector3D(pos.Location.X, pos.Location.Y, pos.Location.Z);
                Matrix3D matrix = new Matrix3D(RubiksCube.GetRubiksCube().Position.RMatrix);
                matrix.Invert();
                location.Transform(matrix);
                location.Normalize();

                if (location.B < y)
                {
                    y = location.B;
                    retLayer = layer;
                }
            }

            if (retLayer != null)
                RubiksCube.GetRubiksCube().SelectedLayer = retLayer;
        }

        public void ExecuteSelectLeft()
        {
            if (RubiksCube.GetRubiksCube().LayerRotating) return;

            float x = 0.0f;
            AbstractLayer retLayer = null;

            foreach (AbstractLayer layer in this.Layers)
            {
                Position pos = layer.GetCube(layer.MiddlePosition).Position.Clone();
                Vector3D location = new Vector3D(pos.Location.X, pos.Location.Y, pos.Location.Z);
                Matrix3D matrix = new Matrix3D(RubiksCube.GetRubiksCube().Position.RMatrix);
                matrix.Invert();
                location.Transform(matrix);
                location.Normalize();

                if (location.A < x)
                {
                    x = location.A;
                    retLayer = layer;
                }
            }

            if (retLayer != null)
                RubiksCube.GetRubiksCube().SelectedLayer = retLayer;
        }

        public void ExecuteSelectNone()
        {
            if (RubiksCube.GetRubiksCube().LayerRotating) return;

            RubiksCube.GetRubiksCube().SelectedLayer = null;
        }

        public void ExecuteSelectRight()
        {
            if (RubiksCube.GetRubiksCube().LayerRotating) return;

            float x = 0.0f;
            AbstractLayer retLayer = null;

            foreach (AbstractLayer layer in this.Layers)
            {
                Position pos = layer.GetCube(layer.MiddlePosition).Position.Clone();
                Vector3D location = new Vector3D(pos.Location.X, pos.Location.Y, pos.Location.Z);
                Matrix3D matrix = new Matrix3D(RubiksCube.GetRubiksCube().Position.RMatrix);
                matrix.Invert();
                location.Transform(matrix);
                location.Normalize();

                if (location.A > x)
                {
                    x = location.A;
                    retLayer = layer;
                }
            }

            if (retLayer != null)
                RubiksCube.GetRubiksCube().SelectedLayer = retLayer;
        }

        public void ExecuteSelectTop()
        {
            if (RubiksCube.GetRubiksCube().LayerRotating) return;

            float y = 0.0f;
            AbstractLayer retLayer = null;

            foreach (AbstractLayer layer in this.Layers)
            {
                Position pos = layer.GetCube(layer.MiddlePosition).Position.Clone();
                Vector3D location = new Vector3D(pos.Location.X, pos.Location.Y, pos.Location.Z);
                Matrix3D matrix = new Matrix3D(RubiksCube.GetRubiksCube().Position.RMatrix);
                matrix.Invert();
                location.Transform(matrix);
                location.Normalize();

                if (location.B > y)
                {
                    y = location.B;
                    retLayer = layer;
                }
            }

            if (retLayer != null)
                RubiksCube.GetRubiksCube().SelectedLayer = retLayer;
        }

        #endregion

        #region Rotation to Layer Methods

        public void ExecuteRotateToBackLayer()
        {
            RubiksCube.GetRubiksCube().RotateToLayer(Vector3D.YAxis, 180.0f);
        }

        public void ExecuteRotateToBottomLayer()
        {
            RubiksCube.GetRubiksCube().RotateToLayer(new Vector3D(-1.0f, 0.0f, 0.0f), 90.0f);
        }

        public void ExecuteRotateToFrontLayer()
        {
            RubiksCube.GetRubiksCube().RotateToLayer(Vector3D.XAxis, 0.0f);
        }

        public void ExecuteRotateToLeftLayer()
        {
            RubiksCube.GetRubiksCube().RotateToLayer(Vector3D.YAxis, 90.0f);
        }

        public void ExecuteRotateToRightLayer()
        {
            RubiksCube.GetRubiksCube().RotateToLayer(new Vector3D(0.0f, -1.0f, 0.0f), 90.0f);
        }

        public void ExecuteRotateToTopLayer()
        {
            RubiksCube.GetRubiksCube().RotateToLayer(Vector3D.XAxis, 90.0f);
        }

        #endregion

        #endregion
    }
}
