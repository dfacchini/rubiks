﻿using System;

using Tao.Glfw;
using Tao.OpenGl;
using RubiksChallenge.Entities.CubeStructure;
using RubiksChallenge.Geometry;
using RubiksChallenge.Environment;

namespace RubiksChallenge.UserInput 
{
    public class UserInputManager
    {
        #region Constructor

        private UserInputManager()
        {
            this.SetBindings();            
            Glfw.glfwEnable(Glfw.GLFW_KEY_REPEAT);
        }

        #endregion

        #region Singleton

        private static UserInputManager instance;

        public static UserInputManager GetManager()
        {
            if (instance == null)
                instance = new UserInputManager();
            return instance;
        }

        #endregion        

        #region Delegates

        public delegate void AntiAliasingLevelChangedHandler(bool raiseLevel);
        public delegate void WindowTypeChangedHandler();
        public delegate int CloseRequestedHandler();

        #endregion

        #region Events

        public event AntiAliasingLevelChangedHandler OnAntiAliasingLevelChanged;
        public event WindowTypeChangedHandler OnWindowTypeChanged;
        public event CloseRequestedHandler OnCloseRequested;

        #endregion

        #region Private Fields

        private int downBind;
        private int leftBind;
        private int rightBind;
        private int upBind;

        private int selectBottomLayerBind;
        private int selectLeftLayerBind;
        private int selectRightLayerBind;
        private int selectTopLayerBind;

        private int rotateBottomLayerPosBind;
        private int rotateBottomLayerNegBind;
        private int rotateLeftLayerPosBind;
        private int rotateLeftLayerNegBind;
        private int rotateRightLayerPosBind;
        private int rotateRightLayerNegBind;
        private int rotateTopLayerPosBind;
        private int rotateTopLayerNegBind;

        private int rotateToBackLayerBind;
        private int rotateToBottomLayerBind;
        private int rotateToFrontLayerBind;
        private int rotateToLeftLayerBind;
        private int rotateToRightLayerBind;
        private int rotateToTopLayerBind;

        private int increaseEyeSeparationBind;
        private int relaxEyeSeparationBind;
        private int resetEyeSeparationBind;
                
        private bool downPressed;
        private bool leftPressed;
        private bool rightPressed;
        private bool upPressed;

        private bool selectBottomLayerPressed;
        private bool selectLeftLayerPressed;
        private bool selectRightLayerPressed;
        private bool selectTopLayerPressed;

        private bool increaseEyeSeparationPressed;
        private bool relaxEyeSeparationPressed;
        private bool resetEyeSeparationPressed;

        #endregion

        #region Private Methods

        private void AntialiasingLevelChangedEventHandler(bool raiseLevel)
        {
            if (this.OnAntiAliasingLevelChanged != null)
                this.OnAntiAliasingLevelChanged(raiseLevel);
        }

        private void CloseRequestedEventHandler()
        {
            if (this.OnCloseRequested != null)
                this.OnCloseRequested();
        }

        private void HandleKeyPress(int key)
        {
            if (this.downBind == key)
                this.downPressed = true;
            if (this.leftBind == key)
                this.leftPressed = true;
            if (this.rightBind == key)
                this.rightPressed = true;
            if (this.upBind == key)
                this.upPressed = true;

            if (this.selectBottomLayerBind == key)
                this.selectBottomLayerPressed = true;
            if (this.selectLeftLayerBind == key)
                this.selectLeftLayerPressed = true;
            if (this.selectRightLayerBind == key)
                this.selectRightLayerPressed = true;
            if (this.selectTopLayerBind == key)
                this.selectTopLayerPressed = true;

            if (this.increaseEyeSeparationBind == key)
                this.increaseEyeSeparationPressed = true;
            if (this.relaxEyeSeparationBind == key)
                this.relaxEyeSeparationPressed = true;
            if (this.resetEyeSeparationBind == key)
                this.resetEyeSeparationPressed = true;

            switch (key)
            {
                case Glfw.GLFW_KEY_F12:
                    this.AntialiasingLevelChangedEventHandler(true);
                    return;
                case Glfw.GLFW_KEY_F11:
                    this.AntialiasingLevelChangedEventHandler(false);
                    return;
                case Glfw.GLFW_KEY_F10:
                    if (AnaglyphStereoscopy.GetAnaglyphStereoscopy().Active)
                        AnaglyphStereoscopy.GetAnaglyphStereoscopy().Deactivate();
                    else
                        AnaglyphStereoscopy.GetAnaglyphStereoscopy().Activate();
                    return;
                case Glfw.GLFW_KEY_F9:
                    this.OnWindowTypeChanged();
                    return;
                case Glfw.GLFW_KEY_ESC:
                    this.CloseRequestedEventHandler();
                    return;                
            }

            this.HandleLayerRotationKeys(key);
            this.HandleRotationToLayerKeys(key);            
        }

        private void HandleKeyRelease(int key)
        {
            if (this.downBind == key)
                this.downPressed = false;
            if (this.leftBind == key)
                this.leftPressed = false;
            if (this.rightBind == key)
                this.rightPressed = false;
            if (this.upBind == key)
                this.upPressed = false;

            if (this.selectBottomLayerBind == key)
                this.selectBottomLayerPressed = false;
            if (this.selectLeftLayerBind == key)
                this.selectLeftLayerPressed = false;
            if (this.selectRightLayerBind == key)
                this.selectRightLayerPressed = false;
            if (this.selectTopLayerBind == key)
                this.selectTopLayerPressed = false;

            if (this.increaseEyeSeparationBind == key)
                this.increaseEyeSeparationPressed = false;
            if (this.relaxEyeSeparationBind == key)
                this.relaxEyeSeparationPressed = false;
            if (this.resetEyeSeparationBind == key)
                this.resetEyeSeparationPressed = false;
        }

        private void HandleLayerRotationKeys(int key)
        {
            if ((this.rotateBottomLayerNegBind == key) && (this.selectBottomLayerPressed))
                CubeLayerRotationManager.GetManager().ExecuteBottomNegativeRotation();
            if ((this.rotateBottomLayerPosBind == key) && (this.selectBottomLayerPressed))
                CubeLayerRotationManager.GetManager().ExecuteBottomPositiveRotation();
            if ((this.rotateLeftLayerNegBind == key) && (this.selectLeftLayerPressed))
                CubeLayerRotationManager.GetManager().ExecuteLeftNegativeRotation();
            if ((this.rotateLeftLayerPosBind == key) && (this.selectLeftLayerPressed))
                CubeLayerRotationManager.GetManager().ExecuteLeftPositiveRotation();
            if ((this.rotateRightLayerNegBind == key) && (this.selectRightLayerPressed))
                CubeLayerRotationManager.GetManager().ExecuteRightNegativeRotation();
            if ((this.rotateRightLayerPosBind == key) && (this.selectRightLayerPressed))
                CubeLayerRotationManager.GetManager().ExecuteRightPositiveRotation();
            if ((this.rotateTopLayerNegBind == key) && (this.selectTopLayerPressed))
                CubeLayerRotationManager.GetManager().ExecuteTopNegativeRotation();
            if ((this.rotateTopLayerPosBind == key) && (this.selectTopLayerPressed))
                CubeLayerRotationManager.GetManager().ExecuteTopPositiveRotation();
        }

        private void HandleRotationToLayerKeys(int key)
        {
            if (this.rotateToBackLayerBind == key)
                CubeLayerRotationManager.GetManager().ExecuteRotateToBackLayer();
            if (this.rotateToBottomLayerBind == key)
                CubeLayerRotationManager.GetManager().ExecuteRotateToBottomLayer();
            if (this.rotateToFrontLayerBind == key)
                CubeLayerRotationManager.GetManager().ExecuteRotateToFrontLayer();
            if (this.rotateToLeftLayerBind == key)
                CubeLayerRotationManager.GetManager().ExecuteRotateToLeftLayer();
            if (this.rotateToRightLayerBind == key)
                CubeLayerRotationManager.GetManager().ExecuteRotateToRightLayer();
            if (this.rotateToTopLayerBind == key)
                CubeLayerRotationManager.GetManager().ExecuteRotateToTopLayer();
        }

        private void WindowTypeChangedEventHandler()
        {
            if (this.OnWindowTypeChanged != null)
                this.OnWindowTypeChanged();
        }

        private void SetBindings()
        {
            this.downBind = Glfw.GLFW_KEY_DOWN;
            this.leftBind = Glfw.GLFW_KEY_LEFT;
            this.rightBind = Glfw.GLFW_KEY_RIGHT;
            this.upBind = Glfw.GLFW_KEY_UP;

            this.selectBottomLayerBind = Glfw.GLFW_KEY_KP_2;
            this.selectLeftLayerBind = Glfw.GLFW_KEY_KP_4;
            this.selectRightLayerBind = Glfw.GLFW_KEY_KP_6;
            this.selectTopLayerBind = Glfw.GLFW_KEY_KP_8;

            this.rotateBottomLayerNegBind = Glfw.GLFW_KEY_KP_1;
            this.rotateBottomLayerPosBind = Glfw.GLFW_KEY_KP_3;
            this.rotateLeftLayerNegBind = Glfw.GLFW_KEY_KP_1;
            this.rotateLeftLayerPosBind = Glfw.GLFW_KEY_KP_7;
            this.rotateRightLayerNegBind = Glfw.GLFW_KEY_KP_3;
            this.rotateRightLayerPosBind = Glfw.GLFW_KEY_KP_9;
            this.rotateTopLayerNegBind = Glfw.GLFW_KEY_KP_7;
            this.rotateTopLayerPosBind = Glfw.GLFW_KEY_KP_9;

            this.rotateToBackLayerBind = 65; // a
            this.rotateToBottomLayerBind = 67; //c
            this.rotateToFrontLayerBind = 68; //d
            this.rotateToLeftLayerBind = 83; //s
            this.rotateToRightLayerBind = 70; //f
            this.rotateToTopLayerBind = 69; //e

            this.increaseEyeSeparationBind = Glfw.GLFW_KEY_KP_ADD;
            this.relaxEyeSeparationBind = Glfw.GLFW_KEY_KP_SUBTRACT;
            this.resetEyeSeparationBind = Glfw.GLFW_KEY_KP_MULTIPLY;
        }

        #endregion

        #region Public Methods

        public void HandleNormalKeys(int key, int action)
        {
            if (action == Glfw.GLFW_PRESS)
                this.HandleKeyPress(key);
            else
                this.HandleKeyRelease(key);           
        }

        public void HandleRotation()
        {
            if (this.downPressed)
                CubeRotationManager.GetManager().ExecuteDown();
            if (this.leftPressed)
                CubeRotationManager.GetManager().ExecuteLeft();
            if (this.rightPressed)
                CubeRotationManager.GetManager().ExecuteRight();
            if (this.upPressed)
                CubeRotationManager.GetManager().ExecuteUp();
        }

        public void HandleLayerSelection()
        {
            if (this.selectBottomLayerPressed)
            {
                CubeLayerRotationManager.GetManager().ExecuteSelectBottom();
                return;
            }
            if (this.selectLeftLayerPressed)
            {
                CubeLayerRotationManager.GetManager().ExecuteSelectLeft();
                return;
            }
            if (this.selectRightLayerPressed)
            {
                CubeLayerRotationManager.GetManager().ExecuteSelectRight();
                return;
            }
            if (this.selectTopLayerPressed)
            {
                CubeLayerRotationManager.GetManager().ExecuteSelectTop();
                return;
            }
            CubeLayerRotationManager.GetManager().ExecuteSelectNone();
        }

        public void HandleAnaglyphStereoscopy()
        {
            if (this.increaseEyeSeparationPressed)
                AnaglyphStereoscopy.GetAnaglyphStereoscopy().IncreaseEyeSeparation();
            if (this.relaxEyeSeparationPressed)
                AnaglyphStereoscopy.GetAnaglyphStereoscopy().RelaxEyeSeparation();
            if (this.resetEyeSeparationPressed)
                AnaglyphStereoscopy.GetAnaglyphStereoscopy().ResetEyeSeparation();
        }

        #endregion
    }
}
