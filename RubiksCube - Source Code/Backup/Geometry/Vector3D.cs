using System;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Drawing;
using System.Collections;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Xml;

namespace RubiksChallenge.Geometry
{
    /// <summary>
    /// Represents a location in 3-space
    /// </summary>
    public struct Vector3D : ICloneable
    {
        // ================================================================================

        /// <summary>
        /// Creates a new vector set to ( x, y, z )
        /// </summary>
        public Vector3D(float a, float b, float c)
        {
            this.A = a;
            this.B = b;
            this.C = c;
        }

        /// <summary>
        /// Creates a new vector set to ( element[0], element[1], element[2] )
        /// </summary>
        public Vector3D(float[] elements)
        {
            Debug.Assert(elements != null);
            Debug.Assert(elements.Length >= 3);
            this.A = elements[0];
            this.B = elements[1];
            this.C = elements[2];
        }

        /// <summary>
        /// Creates a new vector set to the values of the given vector
        /// </summary>
        public Vector3D(Vector3D vec)
        {
            this.A = vec.A;
            this.B = vec.B;
            this.C = vec.C;
        }

        // ================================================================================

        /// <summary>
        /// Create a new vector set to ( x, y, z )
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        static public Vector3D FromABC(float a, float b, float c)
        {
            return new Vector3D(a, b, c);
        }

        /// <summary>
        /// Create a new vector set to ( element[0], element[1], element[2] )
        /// </summary>
        /// <param name="elements"></param>
        /// <returns></returns>
        static public Vector3D FromABC(float[] elements)
        {
            return new Vector3D(elements);
        }

        // ================================================================================

        /// <summary>
        /// Set the X, Y and Z coordinates of the vector at once.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Set(float a, float b, float c)
        {
            this.A = a;
            this.B = b;
            this.C = c;
        }

        /// <summary>
        /// Set the vector to the same location as the given vector
        /// </summary>
        /// <param name="vec"></param>
        public void Set(Vector3D vec)
        {
            this.A = vec.A;
            this.B = vec.B;
            this.C = vec.C;
        }

        // ================================================================================

        object ICloneable.Clone()
        {
            return new Vector3D(this);
        }

        /// <summary>
        /// Create a copy of this vector
        /// </summary>
        /// <returns></returns>
        public Vector3D Clone()
        {
            return new Vector3D(this);
        }

        // ================================================================================

        /// <summary>
        /// The X component of the vector
        /// </summary>
        public float A;

        /// <summary>
        /// The Y component of the vector
        /// </summary>
        public float B;

        /// <summary>
        /// The Z component of the vector
        /// </summary>
        public float C;

        /// <summary>
        /// An index accessor that maps [0] -> X, [1] -> Y and [2] -> Z.
        /// </summary>
        public float this[int index]
        {
            get
            {
                Debug.Assert(0 <= index && index <= 2);
                if (index <= 1)
                {
                    if (index == 0)
                    {
                        return this.A;
                    }
                    return this.B;
                }
                return this.C;
            }
            set
            {
                Debug.Assert(0 <= index && index <= 2);
                if (index <= 1)
                {
                    if (index == 0)
                    {
                        this.A = value;
                    }
                    else
                    {
                        this.B = value;
                    }
                }
                else
                {
                    this.C = value;
                }
            }
        }

        // ================================================================================

        /// <summary>
        /// Get the magnitude of the vector [i.e. Sqrt( X*X + Y*Y + Z*Z ) ]
        /// </summary>
        /// <returns></returns>
        public float GetMagnitude()
        {
            return (float)Math.Sqrt(A * A + B * B + C * C);
        }

        /// <summary>
        /// Get the squared magnitude of the vector [i.e. ( X*X + Y*Y + Z*Z ) ]
        /// </summary>
        /// <returns></returns>
        public float GetMagnitudeSquared()
        {
            return A * A + B * B + C * C;
        }

        // ================================================================================

        /// <summary>
        /// Get the vector with the shortest magnitude
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        static public Vector3D Min(Vector3D a, Vector3D b)
        {
            return (a.GetMagnitude() >= b.GetMagnitude()) ? b : a;
        }

        /// <summary>
        /// Get the vector with the greatest magnitude
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        static public Vector3D Max(Vector3D a, Vector3D b)
        {
            return (a.GetMagnitude() >= b.GetMagnitude()) ? a : b;
        }

        /// <summary>
        /// Get the smallest of each of the components in vector a and vector b
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        static public Vector3D MinABC(Vector3D a, Vector3D b)
        {
            return new Vector3D(
                Math.Min(a.A, b.A),
                Math.Min(a.B, b.B),
                Math.Min(a.C, b.C));
        }

        /// <summary>
        /// Get the greatest of each of the components in vector a and vector b
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        static public Vector3D MaxABC(Vector3D a, Vector3D b)
        {
            return new Vector3D(
                Math.Max(a.A, b.A),
                Math.Max(a.B, b.B),
                Math.Max(a.C, b.C));
        }

        // ================================================================================

        /// <summary>
        /// Get the vector's unit vector.
        /// </summary>
        /// <returns></returns>
        public Vector3D GetUnit()
        {
            Vector3D vec = new Vector3D(this);
            vec.Normalize();
            return vec;
        }

        /// <summary>
        /// Scale point so that the magnitude is one
        /// </summary>
        public void Normalize()
        {
            float length = GetMagnitude();
            if (length == 0)
                return;
            this.A = A / length;
            this.B = B / length;
            this.C = C / length;
        }

        /// <summary>
        /// Transform the point
        /// </summary>
        /// <param name="xfrm"></param>
        public void Transform(Matrix3D xfrm)
        {
            float x = A, y = B, z = C;
            A = x * xfrm[0] + y * xfrm[4] + z * xfrm[8] + xfrm[12];
            B = x * xfrm[1] + y * xfrm[5] + z * xfrm[9] + xfrm[13];
            C = x * xfrm[2] + y * xfrm[6] + z * xfrm[10] + xfrm[14];
        }

        // ================================================================================

        /// <summary>
        /// Convert the point into the array 'new float[]{ X, Y, Z }'.  Note that this
        /// function causes a new float[] array to be allocated with each call.  Thus it 
        /// is somewhat inefficient.
        /// </summary>
        /// <param name="vec"></param>
        /// <returns></returns>
        static public explicit operator float[](Vector3D vec)
        {
            float[] elements = new float[3];
            elements[0] = vec.A;
            elements[1] = vec.B;
            elements[2] = vec.C;
            return elements;
        }

        // ================================================================================

        /// <summary>
        /// A human understandable descrivecion of the point.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("( x={0}, y={1}, z={2} )", A, B, C);
        }

        // ================================================================================

        /// <summary>
        /// Are two points equal?
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        static public bool operator ==(Vector3D a, Vector3D b)
        {
            return (a.A == b.A) && (a.B == b.B) && (a.C == b.C);
        }

        /// <summary>
        /// Are two point not equal?
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        static public bool operator !=(Vector3D a, Vector3D b)
        {
            return (a.A != b.A) || (a.B != b.B) || (a.C != b.C);
        }

        /// <summary>
        /// Is given object equal to current point?
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public override bool Equals(object o)
        {
            if (o is Vector3D)
            {
                Vector3D vec = (Vector3D)o;
                return (this.A == vec.A) && (this.B == vec.B) && (this.C == vec.C);
            }
            return false;
        }

        /// <summary>
        /// Get the hashcode of the point
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return this.A.GetHashCode() ^ (this.B.GetHashCode() ^ (~this.C.GetHashCode()));
        }

        // ================================================================================

        /// <summary>
        /// Do nothing.
        /// </summary>
        /// <param name="vec"></param>
        /// <returns></returns>
        static public Vector3D operator +(Vector3D vec)
        {
            return Vector3D.FromABC(+vec.A, +vec.B, +vec.C);
        }
        
        /// <summary>
        /// Invert the direction of the point.  Result is ( -vec.X, -vec.Y, -vec.Z ).
        /// </summary>
        /// <param name="vec"></param>
        /// <returns></returns>
        static public Vector3D operator -(Vector3D vec)
        {
            return Vector3D.FromABC(-vec.A, -vec.B, -vec.C);
        }

        /// <summary>
        /// Multiply vector vec by f.  Result is ( vec.X*f, vec.Y*f, vec.Z*f ).
        /// </summary>
        /// <param name="f"></param>
        /// <param name="vec"></param>
        /// <returns></returns>
        static public Vector3D operator *(float f, Vector3D vec)
        {
            return Vector3D.FromABC(vec.A * f, vec.B * f, vec.C * f);
        }

        /// <summary>
        /// Multiply vector vec by f.  Result is ( vec.X*f, vec.Y*f, vec.Z*f ).
        /// </summary>
        /// <param name="f"></param>
        /// <param name="vec"></param>
        /// <returns></returns>
        static public Vector3D operator *(Vector3D vec, float f)
        {
            return Vector3D.FromABC(vec.A * f, vec.B * f, vec.C * f);
        }

        /// <summary>
        /// Divide vector vec by f.  Result is ( vec.X/f, vec.Y/f, vec.Z/f ).
        /// </summary>
        /// <param name="vec"></param>
        /// <param name="f"></param>
        /// <returns></returns>
        static public Vector3D operator /(Vector3D vec, float f)
        {
            if (f == 0)
            {
                throw new DivideByZeroException("can not divide a vector by zero");
            }
            return Vector3D.FromABC(vec.A / f, vec.B / f, vec.C / f);
        }

        /// <summary>
        /// Add two vectors.  Result is ( a.X + b.X, a.Y + b.Y, a.Z + b.Z )
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        static public Vector3D operator +(Vector3D a, Vector3D b)
        {
            return Vector3D.FromABC(a.A + b.A, a.B + b.B, a.C + b.C);
        }

        /// <summary>
        /// Subtract two vectors.  Result is ( a.X - b.X, a.Y - b.Y, a.Z - b.Z )
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        static public Vector3D operator -(Vector3D a, Vector3D b)
        {
            return Vector3D.FromABC(a.A - b.A, a.B - b.B, a.C - b.C);
        }

        /// <summary>
        /// Transform point vec by xfrm.
        /// </summary>
        /// <param name="xfrm"></param>
        /// <param name="vec"></param>
        /// <returns></returns>
        static public Vector3D operator *(Matrix3D xfrm, Vector3D vec)
        {
            Vector3D vecNew = (Vector3D)vec.Clone();
            vecNew.Transform(xfrm);
            return vecNew;
        }

        // ================================================================================

        /// <summary>
        /// Add p to self.  Much quicker than using '+' operator since no new objects are created.
        /// </summary>
        /// <param name="vec"></param>
        public void Add(Vector3D vec)
        {
            this.A += vec.A;
            this.B += vec.B;
            this.C += vec.C;
        }

        /// <summary>
        /// Subtract p from self.  Much quicker than using '-' operator since no new objects are created.
        /// </summary>
        /// <param name="vec"></param>
        public void Subtract(Vector3D vec)
        {
            this.A -= vec.A;
            this.B -= vec.B;
            this.C -= vec.C;
        }

        /// <summary>
        /// Multiply self by f.  Much quicker than using '*' operator since no new objects are created.
        /// </summary>
        /// <param name="f"></param>
        public void Multiply(float f)
        {
            this.A *= f;
            this.B *= f;
            this.C *= f;
        }

        /// <summary>
        /// Divide self by f.  Much quicker than using '/' operator since no new objects are created.
        /// </summary>
        /// <param name="f"></param>
        public void Divide(float f)
        {
            if (f == 0)
            {
                throw new DivideByZeroException("can not divide a vector by zero");
            }
            this.A /= f;
            this.B /= f;
            this.C /= f;
        }

        // ================================================================================

        /// <summary>
        /// Determine the angle between two vectors
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>        
        /// <returns></returns> 
        static public float Angle(Vector3D a, Vector3D b)
        {
            float dividend = Vector3D.DotProduct(a, b);
            float divisor = (a.GetMagnitude() * b.GetMagnitude());

            if (divisor == 0)
                return (float)Math3D.Deg(Math.Acos(0));
            else
                return (float)Math3D.Deg(Math.Acos(dividend / divisor));
        }

        /// <summary>
        /// Calculate the dot project (i.e. inner product) of a and b.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        static public float Dot(Vector3D a, Vector3D b)
        {
            return DotProduct(a, b);
        }

        /// <summary>
        /// Calculate the dot project (i.e. inner product) of a and b.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        static public float DotProduct(Vector3D a, Vector3D b)
        {
            // make it easier to see what is happening
            float x0 = a.A, y0 = a.B, z0 = a.C;
            float x1 = b.A, y1 = b.B, z1 = b.C;
            return x0 * x1 + y0 * y1 + z0 * z1;
        }

        /// <summary>
        /// Calculate the cross product (i.e. outer product) of a and b
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        static public Vector3D Cross(Vector3D a, Vector3D b)
        {
            return CrossProduct(a, b);
        }

        /// <summary>
        /// Calculate the cross product (i.e. outer product) of a and b
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        static public Vector3D CrossProduct(Vector3D a, Vector3D b)
        {
            // make it easier to see what is happening
            float x0 = a.A, y0 = a.B, z0 = a.C;
            float x1 = b.A, y1 = b.B, z1 = b.C;
            return Vector3D.FromABC(y0 * z1 - z0 * y1, z0 * x1 - x0 * z1, x0 * y1 - y0 * x1);
        }

        /// <summary>
        /// Determine whether sequency a-b-c is Clockwise, Counterclockwise or Collinear
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <returns></returns> 
        static public int Orientation(Vector3D a, Vector3D b, Vector3D c)
        {
            float length = Vector3D.CrossProduct(b - a, b - c).GetMagnitude();
            if (length > 0)
            {
                return 1;
            }
            if (length < 0)
            {
                return -1;
            }
            if (length == 0)
            {
                return 0;
            }
            Debug.Fail("should never get here");
            return 0;
        }        

        // ================================================================================

        /// <summary>
        /// Zero (0,0,0)
        /// </summary>
        static public readonly Vector3D Zero = Vector3D.FromABC(0, 0, 0);

        /// <summary>
        /// Origin (0,0,0)
        /// </summary>
        static public readonly Vector3D Origin = Vector3D.FromABC(0, 0, 0);

        /// <summary>
        /// X-axis unit vector (1,0,0)
        /// </summary>
        static public readonly Vector3D XAxis = Vector3D.FromABC(1, 0, 0);

        /// <summary>
        /// Y-axis unit vector (0,1,0)
        /// </summary>
        static public readonly Vector3D YAxis = Vector3D.FromABC(0, 1, 0);

        /// <summary>
        /// Z-axis unit vector (0,0,1)
        /// </summary>
        static public readonly Vector3D ZAxis = Vector3D.FromABC(0, 0, 1);

        // ================================================================================
    }
}
