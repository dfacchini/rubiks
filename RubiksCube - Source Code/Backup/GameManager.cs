﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using RubiksChallenge.Environment;
using RubiksChallenge.GameStates;
using RubiksChallenge.UserInput;

using Tao.Glfw;
using Tao.OpenGl;

namespace RubiksChallenge
{
    public class GameManager
    {
        #region Constructor

        public GameManager()
        {            
        } 

        #endregion

        #region Consts

        const int TICKS_PER_SECOND = 50;
        const int SKIP_TICKS = 1000 / TICKS_PER_SECOND;
        const int MAX_FRAMESKIP = 10;

        const string INGAMESTATE = "ingamestate";
        const string INSHIPEDITION = "inshipedition";

        #endregion

        #region Delegates

        private Glfw.GLFWwindowclosefun closeFunction;
        private Glfw.GLFWkeyfun keyboardFunction;        
        private Glfw.GLFWwindowsizefun reshapeFunction;

        #endregion        

        #region Private Fields

        private int antiAliasingLevel = 4;        
        private List<IGameState> gameStates = new List<IGameState>();
        private IGameState currentState;
        private bool gameRunning = true;
        private bool gameHalted = false;
        private int windowType = Glfw.GLFW_WINDOW;
        private int windowX = 0;
        private int windowY = 0;
        private int windowHeight = 600;
        private int windowWidth = 800;
                
        #endregion        
        
        #region Private Methods

        private void CenterWindow()
        {
            Rectangle screen = SystemInformation.WorkingArea;
            int height = (int)screen.Height / 2;
            int width = (int)screen.Width / 2;
            this.windowX = width - 400;
            this.windowY = height - 300;
        }

        private void ChangeAntiAliasingLevel(bool raiseLevel)
        {
            if (raiseLevel)
            {
                if (this.antiAliasingLevel < 6)
                    this.antiAliasingLevel += 1;
                else
                    return;
            }
            else
            {
                if (this.antiAliasingLevel > 1)
                    this.antiAliasingLevel -= 1;
                else
                    return;
            }

            this.ReInitManager();            
        }

        private void ChangeWindowType()
        {
            if (this.windowType == Glfw.GLFW_WINDOW)
            {
                this.windowWidth = SystemInformation.PrimaryMonitorSize.Width;
                this.windowHeight = SystemInformation.PrimaryMonitorSize.Height;
                this.windowType = Glfw.GLFW_FULLSCREEN;
            }
            else
            {
                if (AnaglyphStereoscopy.GetAnaglyphStereoscopy().Active) return;

                this.windowWidth = 800;
                this.windowHeight = 600;
                this.windowType = Glfw.GLFW_WINDOW;
            }
            this.ReInitManager();
        }

        private int FinishManager()
        {
            this.gameRunning = false;
            Glfw.glfwSetWindowCloseCallback(null);
            Glfw.glfwSetKeyCallback(null);
            Glfw.glfwSetWindowSizeCallback(null);
            Glfw.glfwTerminate();
            return 0;
        }

        private void GameLoop()
        {
            while (this.gameRunning)
            {
                if (!this.gameHalted)
                {
                    if (this.antiAliasingLevel > 0)
                        Gl.glEnable(Gl.GL_MULTISAMPLE_ARB);

                    if (AnaglyphStereoscopy.GetAnaglyphStereoscopy().Active)
                    {
                        if (this.windowType != Glfw.GLFW_FULLSCREEN)
                            this.ChangeWindowType();
                        this.currentState.RenderAnaglyphStereoscopy(this);
                    }
                    else
                        this.currentState.Render(this);

                    if (this.antiAliasingLevel > 0)    
                        Gl.glDisable(Gl.GL_MULTISAMPLE_ARB);

                    this.currentState.Update(this);
                }
            }
        }

        private void InitManager()
        {
            this.CenterWindow();
                Glfw.glfwInit();
            Glfw.glfwOpenWindowHint(Glfw.GLFW_FSAA_SAMPLES, this.antiAliasingLevel);
            if (Glfw.glfwOpenWindow(windowWidth, windowHeight, 0, 0, 0, 0, 16, 0, this.windowType) == Gl.GL_FALSE)
            {
                this.FinishManager();
                return;
            }
            
            Glfw.glfwSetWindowTitle("Rubik`s Challenge 2008 - JBT Digital Entertainment");
            Glfw.glfwSetWindowPos(this.windowX, this.windowY);

            closeFunction = new Glfw.GLFWwindowclosefun(this.FinishManager);
            Glfw.glfwSetWindowCloseCallback(this.closeFunction);

            keyboardFunction = new Glfw.GLFWkeyfun(UserInputManager.GetManager().HandleNormalKeys);
            Glfw.glfwSetKeyCallback(this.keyboardFunction);

            reshapeFunction = new Glfw.GLFWwindowsizefun(this.Reshape); 
            Glfw.glfwSetWindowSizeCallback(this.reshapeFunction);

            UserInputManager.GetManager().OnAntiAliasingLevelChanged +=
                new UserInputManager.AntiAliasingLevelChangedHandler(ChangeAntiAliasingLevel);

            UserInputManager.GetManager().OnCloseRequested += 
                new UserInputManager.CloseRequestedHandler(FinishManager);

            UserInputManager.GetManager().OnWindowTypeChanged += 
                new UserInputManager.WindowTypeChangedHandler(ChangeWindowType);
        }

        private void ReInitManager()
        {
            this.gameHalted = true;
            UserInputManager.GetManager().OnAntiAliasingLevelChanged -=
                new UserInputManager.AntiAliasingLevelChangedHandler(this.ChangeAntiAliasingLevel);
            UserInputManager.GetManager().OnCloseRequested -=
                new UserInputManager.CloseRequestedHandler(this.FinishManager);
            UserInputManager.GetManager().OnWindowTypeChanged -=
                new UserInputManager.WindowTypeChangedHandler(this.ChangeWindowType);
            Glfw.glfwTerminate();
            this.InitManager();
            this.currentState.ReInit(this);
            this.gameHalted = false;
        }

        private void InitStates()
        {
            this.AddState(new InGameState());            

            foreach (IGameState state in gameStates)
                state.Init(this);

            this.ChangeToState("ingamestate");            
        }        

        private void Reshape(int width, int height)
        {
            Scene.GetScene().Initialize(width, height);
        }        

        #endregion

        #region Public Methods

        public void AddState(IGameState state)
        {
            if (currentState == null)
                currentState = state;

            gameStates.Add(state);
        }

        public void ChangeToState(String name)
        {
            IGameState newState = null;

            foreach (IGameState state in gameStates)
                if (string.Equals(state.GetName, name))
                    newState = state;

            if (newState == null)
                return;

            currentState.Leave(this);
            currentState = newState;
            currentState.Enter(this);
        }

        public void StartGame()
        {
            this.InitManager();            
            this.InitStates();
            this.GameLoop();
            this.FinishManager();
        }

        #endregion       

        #region Static Method

        [STAThread]
        static void Main()
        {
            GameManager gameManager = new GameManager();
            gameManager.StartGame();
        }

        #endregion
    }
}
