using System;

namespace RubiksChallenge.Entities
{
    public interface IEntityManager
    {
        #region Inteface Methods

        void AddEntity(AbstractEntity entity);        

        void RemoveEntity(AbstractEntity entity);

        #endregion
    }
}
