using System;
using System.Drawing;

using RubiksChallenge.Entities.CubeStructure.Factory;
using RubiksChallenge.Environment;
using RubiksChallenge.Geometry;
using RubiksChallenge.Model;
using RubiksChallenge.Properties;
using RubiksChallenge.UserInput;

using Tao.OpenGl;

namespace RubiksChallenge.Entities
{
    public abstract class AbstractEntity
    {
        #region Constructors

        public AbstractEntity()
        {
            this.Position = new Position();
        }

        public AbstractEntity(string resourceName, Colors color) : this()
        {
            this.Color = color;
            this.Model = ObjLoader.GetLoader().LoadObj(resourceName, this.GetColor(color), this.GetAnaglyphStereoscopyColor(color));
        }

        #endregion

        #region Attributes and Properties

        private Colors color;
        public Colors Color
        {
            get { return this.color; }
            set { this.color = value; }
        }

        private ObjModel model;
        public ObjModel Model
        {
            get { return this.model; }
            set { this.model = value; }
        }

        private Position position;
        public Position Position
        {
            get { return position; }
            set { position = value; }
        }        

        #endregion

        #region Abstract Methods

        public abstract void ReInit();

        #endregion

        #region Protected Methods

        protected Bitmap GetColor(Colors color)
        {
            switch (color)
            {
                case Colors.Black:
                    return Resources.Black;
                case Colors.Blue:
                    return Resources.Blue;
                case Colors.Green:
                    return Resources.Green;
                case Colors.Orange:
                    return Resources.Orange;
                case Colors.Red:
                    return Resources.Red;
                case Colors.White:
                    return Resources.White;
                case Colors.Yellow:
                    return Resources.Yellow;
                default:
                    return Resources.Black;
            }
        }

        protected Bitmap GetAnaglyphStereoscopyColor(Colors color)
        {
            //switch (color)
            //{
            //    case Colors.Black:
            //        return Resources.Black;
            //    case Colors.Blue:
            //        return Resources.BlueAnaglyphStereoscopy;
            //    case Colors.Green:
            //        return Resources.GreenAnaglyphStereoscopy;
            //    case Colors.Orange:
            //        return Resources.OrangeAnaglyphStereoscopy;
            //    case Colors.Red:
            //        return Resources.RedAnaglyphStereoscopy;
            //    case Colors.White:
            //        return Resources.WhiteAnaglyphStereoscopy;
            //    case Colors.Yellow:
            //        return Resources.YellowAnaglyphStereoscopy;
            //    default:
            //        return Resources.Black;
            //}            
            switch (color)
            {
                case Colors.Black:
                    return Resources.WhiteAnaglyphStereoscopy;
                default:
                    return Resources.OrangeAnaglyphStereoscopy;
            }    
        }
        
        #endregion

        #region Public Virtual Methods

        public virtual void Render()
        {
            Gl.glPushMatrix();

            Gl.glTranslated(this.Position.Location.X, this.Position.Location.Y, this.Position.Location.Z);
            Gl.glRotatef(this.Position.Rotation, this.Position.Axis.A, this.Position.Axis.B, this.Position.Axis.C);

            if (this.Model != null)
                this.Model.Render();

            Gl.glPopMatrix();
        }

        public virtual void Update(IEntityManager manager)
        {            
        }

        #endregion
    }
}
