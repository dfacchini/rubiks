using System;

using RubiksChallenge.Entities.CubeStructure.Factory;
using RubiksChallenge.Entities.CubeStructure.Layers;
using RubiksChallenge.Geometry;
using RubiksChallenge.Model;
using RubiksChallenge.Properties;

using Tao.OpenGl;

namespace RubiksChallenge.Entities.CubeStructure
{
    public class Cube : AbstractEntity
    {
        #region Constructor

        public Cube()
            : base(resourceName, Colors.Black)
        {            
        }

        #endregion

        #region Consts

        private const string resourceName = "RubiksChallenge.Resources.Cublet.obj";

        #endregion

        #region Attributes and Properties

        private CubeFace[] faces;
        public CubeFace[] Faces
        {
            get { return this.faces; }
            set { this.faces = value; }
        }

        private bool selected;
        public bool Selected
        {
            get { return this.selected; }
            set { this.selected = value; }
        }

        #endregion

        #region Overriden Methods

        public override void Render()
        {           
            Gl.glPushMatrix();

            Gl.glTranslated(this.Position.Location.X, this.Position.Location.Y, this.Position.Location.Z);
            Gl.glRotatef(this.Position.Rotation, this.Position.Axis.A, this.Position.Axis.B, this.Position.Axis.C);
            
            if (this.Model != null)
                this.Model.Render();

            if (this.Faces != null)
                for (int i = 0; i < this.Faces.Length; i++)
                    this.Faces[i].Render();            

            Gl.glPopMatrix();
        }

        public override void ReInit()
        {
            foreach (CubeFace face in this.Faces)
                face.ReInit();
            this.Model = ObjLoader.GetLoader().LoadObj(resourceName, this.GetColor(this.Color), this.GetAnaglyphStereoscopyColor(this.Color));
        }

        #endregion
    }
}
