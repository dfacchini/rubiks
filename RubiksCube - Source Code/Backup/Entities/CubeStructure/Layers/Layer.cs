using System;

namespace RubiksChallenge.Entities.CubeStructure.Layers
{
    public enum Layer
    {
        Back,
        Bottom,
        Front,
        Left,
        None,
        Right,     
        Top
    }
}
